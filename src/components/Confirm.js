import React from "react";

function Confirm({ nextStep, prevStep, values }) {
  const { firstName, lastName, email, occupation, city, bio } = values;
  const handleContinue = e => {
    e.preventDefault();
    // handle form processing and server processes
    nextStep();
  };
  const handleReturn = e => {
    e.preventDefault();
    prevStep();
  };
  return (
    <div className="confirm--container">
      <h1>Confirm all details are correct</h1>
      <div className="confirm--wrapper">
        <div className="user--details">
          <span className="heading">
            <hr />
            <h5>User Details</h5>
          </span>
          <div className="card">
            <span>
              <b>First Name</b> : {firstName}
            </span>
            <span>
              <b>Last Name</b> : {lastName}
            </span>
            <span>
              <b>Email</b> : {email}
            </span>
          </div>
        </div>
        <div className="user--details">
          <span className="heading">
            <hr />
            <h5> Personal Details</h5>
          </span>
          <div className="card">
            <span>
              <b>Occupation</b> : {occupation}
            </span>
            <span>
              <b>City</b> : {city}
            </span>
            <span>
              <b>Bio</b> : {bio}
            </span>
          </div>
        </div>
        <button onClick={handleReturn} className="btn btn--return">
          Back
        </button>
        <button onClick={handleContinue} className="btn btn--continue">
          Confirm & Submit
        </button>
      </div>
    </div>
  );
}

export default Confirm;
