import React from "react";

function Success() {
  return (
    <div className="success--container">
      <div className="success--card">
        <h1>Thank you. Your Information has been sent successfully</h1>
      </div>
    </div>
  );
}

export default Success;
