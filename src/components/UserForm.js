import React, { useState } from "react";
import FormUserDetails from "./FormUserDetails";
import FormPersonalDetails from "./FormPersonalDetails";
import Confirm from "./Confirm";
import Success from "./Success";

function UserForm() {
  const [value, setValue] = useState({
    step: 1,
    firstName: "",
    lastName: "",
    email: "",
    occupation: "",
    city: "",
    bio: ""
  });

  // proceed to next step
  const nextStep = () => {
    const { step } = value;
    setValue({
      ...value,
      step: step + 1
    });
  };

  // proceed to previous step
  const prevStep = () => {
    const { step } = value;
    setValue({
      ...value,
      step: step - 1
    });
  };

  // handle change
  const handleChange = e => {
    setValue({ ...value, [e.target.name]: e.target.value });
  };

  const { step, firstName, lastName, email, occupation, city, bio } = value;

  const values = {
    firstName,
    lastName,
    email,
    occupation,
    city,
    bio
  };

  switch (step) {
    case 1:
      return (
        <FormUserDetails
          nextStep={nextStep}
          handleChange={handleChange}
          values={values}
        ></FormUserDetails>
      );
    case 2:
      return (
        <FormPersonalDetails
          nextStep={nextStep}
          handleChange={handleChange}
          prevStep={prevStep}
          values={values}
        ></FormPersonalDetails>
      );
    case 3:
      return (
        <Confirm
          values={values}
          nextStep={nextStep}
          prevStep={prevStep}
        ></Confirm>
      );
    case 4:
      return <Success></Success>;
    default:
      return;
  }
}

export default UserForm;
