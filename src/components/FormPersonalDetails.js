import React from "react";

function FormPersonalDetails({ values, nextStep, prevStep, handleChange }) {
  const { occupation, city, bio } = values;

  const handleContinue = e => {
    e.preventDefault();
    nextStep();
  };

  const handleReturn = e => {
    e.preventDefault();
    prevStep();
  };
  return (
    <div className="user--detail">
      <h1>Enter Personal Details</h1>
      <div className="form--group">
        <label htmlFor="occupation">Occupation</label>
        <input
          type="text"
          defaultValue={occupation}
          name="occupation"
          className="form--control"
          placeholder="occupation"
          onChange={handleChange}
        />
      </div>
      <div className="form--group">
        <label htmlFor="city">City</label>
        <input
          type="text"
          defaultValue={city}
          name="city"
          className="form--control"
          placeholder="city"
          onChange={handleChange}
        />
      </div>
      <div className="form--group">
        <label htmlFor="bio">Bio</label>
        <input
          type="text"
          defaultValue={bio}
          name="bio"
          className="form--control"
          placeholder="bio"
          onChange={handleChange}
        />
      </div>
      <br />
      <button onClick={handleReturn} className="btn btn--return">
        Back
      </button>
      <button onClick={handleContinue} className="btn btn--continue">
        Continue
      </button>
    </div>
  );
}

export default FormPersonalDetails;
