import React from "react";

function FormUserDetails({ values, nextStep, handleChange }) {
  const { firstName, lastName, email } = values;
  const handleContinue = e => {
    e.preventDefault();
    nextStep();
  };
  return (
    <div className="user--detail">
      <h1>Enter User Details</h1>
      <div className="form--group">
        <label htmlFor="firstname">FirstName</label>
        <input
          type="text"
          defaultValue={firstName}
          name="firstName"
          className="form--control"
          placeholder="first name"
          onChange={handleChange}
        />
      </div>
      <div className="form--group">
        <label htmlFor="lastname">LastName</label>
        <input
          type="text"
          defaultValue={lastName}
          name="lastName"
          className="form--control"
          placeholder="last name"
          onChange={handleChange}
        />
      </div>
      <div className="form--group">
        <label htmlFor="email">Email</label>
        <input
          type="email"
          defaultValue={email}
          name="email"
          className="form--control"
          placeholder="email"
          onChange={handleChange}
        />
      </div>
      <br />
      <button onClick={handleContinue} className="btn btn--continue">
        Continue
      </button>
    </div>
  );
}

export default FormUserDetails;
